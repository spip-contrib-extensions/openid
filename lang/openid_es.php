<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/openid?lang_cible=es
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// E
	'erreur_librairies' => 'Imposible encontrar las bibliotecas de autenticación OpenID',
	'erreur_openid' => 'Esta dirección OpenID no parece válida',
	'erreur_openid_info_manquantes' => 'Su proveedor OpenID no nos ha enviado toda la información necesaria',

	// F
	'form_forum_indiquer_openid' => 'Usted puede registrarse en este sitio indicando un identificador OpenID.',
	'form_forum_openid' => 'Registro por OpenID',
	'form_login_openid' => 'Usted puede utilizar también un identificador OpenID (<a href="http://fr.wikipedia.org/wiki/OpenID" target="_blank">aide</a>)',
	'form_login_openid_inconnu' => 'Esta dirección OpenID es desconocida. Corríjala, o regístrela en su perfil',
	'form_login_openid_ok' => 'Este identificador utiliza OpenID.',
	'form_login_openid_pass' => 'Utilizar una contraseña',
	'form_login_statut_nouveau' => 'En la primera conexión, ha de utilizar la contraseña que se le ha enviado por correo electrónico',
	'form_pet_votre_openid' => 'Su dirección OpenID',

	// O
	'openid' => 'OpenID',

	// U
	'utilisateur_inconnu' => 'Usuario desconocido en el sitio',

	// V
	'verif_refusee' => 'Verificación rechazada'
);
