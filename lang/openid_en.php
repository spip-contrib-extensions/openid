<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/openid?lang_cible=en
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// E
	'erreur_librairies' => 'Unable to locate OpenID authentification libraries',
	'erreur_openid' => 'OpenID authentication error: have you entered a valid OpenID?',
	'erreur_openid_info_manquantes' => 'Your OpenID provider did not send all the required information',

	// F
	'form_forum_indiquer_openid' => 'You can sign up on this site with your OpenID address',
	'form_forum_openid' => 'Sign up with OpenID',
	'form_login_openid' => 'You can use your <a href="http://en.wikipedia.org/wiki/OpenID">OpenID</a> as login',
	'form_login_openid_inconnu' => 'This OpenID address is unknown. Correct, or save it in your profile.',
	'form_login_openid_ok' => 'This login uses OpenID.',
	'form_login_openid_pass' => 'Connect with password.',
	'form_login_statut_nouveau' => 'For your first connection, you must use the password that was sent by email',
	'form_pet_votre_openid' => 'OpenID login',

	// O
	'openid' => 'OpenID',

	// U
	'utilisateur_inconnu' => 'Unknown user',

	// V
	'verif_refusee' => 'Authentication request cancelled'
);
