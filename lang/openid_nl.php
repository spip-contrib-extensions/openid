<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/openid?lang_cible=nl
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// E
	'erreur_librairies' => 'De authenticatie-bibliotheek van OpenID kan niet worden gevonden',
	'erreur_openid' => 'Dit OpenID-adres lijkt niet geldig',
	'erreur_openid_info_manquantes' => 'Je OpenID-leverancier heeft je niet alle benodigde informatie toegezonden',

	// F
	'form_forum_indiquer_openid' => 'Je kunt je op deze site inschrijven met behulp van een OpenID.',
	'form_forum_openid' => 'Inschrijving door OpenID',
	'form_login_openid' => 'Je kunt je ook identificeren met een OpenID (<a href="http://nl.wikipedia.org/wiki/OpenID" target="_blank">aide</a>)',
	'form_login_openid_inconnu' => 'Dit OpenID-adres is onbekend. Pas het aan, of registreer het in je profiel.',
	'form_login_openid_ok' => 'Deze gebruiker maakt gebruik van OpenID.',
	'form_login_openid_pass' => 'Een wachtwoord gebruiken',
	'form_login_statut_nouveau' => 'Gebruik voor de eerste connectie het per email toegezonden wachtwoord',
	'form_pet_votre_openid' => 'Je OpenID-adres',

	// O
	'openid' => 'OpenID',

	// U
	'utilisateur_inconnu' => 'Onbekende gebruiker op deze site',

	// V
	'verif_refusee' => 'Verificatie geweigerd'
);
