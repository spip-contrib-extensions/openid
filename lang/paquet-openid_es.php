<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-openid?lang_cible=es
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// O
	'openid_description' => 'Un usuario que desee utilizar su OpenID para acceder al sitio debe introducirlo como dirección de su sitio web en su perfil. A continuación, en lugar de meter su identificador de usuario o dirección de correo electrónico, basta con introducir su OpenID para conectarse.',
	'openid_slogan' => 'Autentificación de los autores y visitantes en ayuda de su OpenID'
);
