<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/openid?lang_cible=de
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// E
	'erreur_librairies' => 'Die für OpenID-Identifikation erforderlichen Programmbibliotheken können nicht gefunden werden.',
	'erreur_openid' => 'Diese OpenID Adresse scheint ungültig zu sein.',
	'erreur_openid_info_manquantes' => 'Ihr OpenID-Provider hat nict alle erforderlichen Informationen übertragen.',

	// F
	'form_forum_indiquer_openid' => 'Sie können sich bei dieser Website unter Angabe einer OpenID anmelden.',
	'form_forum_openid' => 'OpenID Anmeldung',
	'form_login_openid' => 'Sie können auch eine OpenID Identität verwenden (<a href="http://de.wikipedia.org/wiki/OpenID" target="_blank">Hilfe</a>)',
	'form_login_openid_inconnu' => 'Diese OpenID Adresse ist unbekannt. Bitte korrigieren sie sie oder tragen sie in Ihr Profil ein.',
	'form_login_openid_ok' => 'Diese Identität verwendet OpenID.',
	'form_login_openid_pass' => 'Bitte ein Passwot verwenden',
	'form_login_statut_nouveau' => 'Für die erste Anmeldung müssen Sie das Passwort verwenden, dass Sie per Mail erhalten haben.',
	'form_pet_votre_openid' => 'Ihre OpenID Adresse',

	// O
	'openid' => 'OpenID',

	// U
	'utilisateur_inconnu' => 'Benutzer ist bei dieser Website nicht bekannt.',

	// V
	'verif_refusee' => 'Überprüfung fehlgeschlagen'
);
